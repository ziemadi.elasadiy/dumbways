<!DOCTYPE html>
<html>
<head>
	<title>CRUD</title>
</head>
<body>
	<a href="6-customer.html" class="button">Customer</a>
	<a href="6-brand.html" class="button">Brand</a>
	<a href="6-cars.html" class="button">Cars</a>

	<br>
	<br>

	<a href="6-customer.php">Lihat Semua Data</a>

	<br/>
	<h3>Input data baru</h3>
	<form action="6-customer-tambah-aksi.php" method="post">
		<table>
			<tr>
				<td>ID</td>
				<td><input type="text" name="id"></td>
			</tr>
			<tr>
				<td>Nama</td>
				<td><input type="text" name="name"></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><input type="email" name="email"></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td><textarea type="text" name="addresss"></textarea></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Simpan"></td>
			</tr>
		</table>
	</form>
</body>
</html>
