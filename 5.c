# include <stdio.h>

int main ()
{
    int sisi,i,j;
    printf ("-----Program Menampilkan Bingkai Persegi-----\n");
    printf ("\nMasukkan Sisi =  "); scanf("%d", &sisi);

    if(sisi>2)
    {
        for (i=0; i<sisi; i++)
        {
            for(j=0; j<sisi; j++)
            {
                if ((i==0) || (i==sisi-1) || (j==0) || (j== sisi-1))
                {
                    printf("+");
                }
                else if (sisi%2 == 1)
                    {
                        printf ("=");
                    }
                else
                {
                    printf ("");
                }
            }
            printf ("\n");
        }

    }
return 0;
}
