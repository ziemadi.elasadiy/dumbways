<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>CRUD</title>
  </head>
  <body>
    <a href="6-customer.php" class="button">Customer</a>
    <a href="6-brand.php" class="button">Brand</a>
    <a href="6-cars.php" class="button">Cars</a>

    <br>
    <?php
    	if(isset($_GET['pesan'])){
    		$pesan = $_GET['pesan'];
    		if($pesan == "input"){
    			echo "Data berhasil di input.";
    		}else if($pesan == "update"){
    			echo "Data berhasil di update.";
    		}else if($pesan == "hapus"){
    			echo "Data berhasil di hapus.";
    		}
    	}
    	?>

    <br>
    <a href="6-customer-tambah.php" class="button">Tambah</a>

    <h3>Data Customer</h3>
  	<table border="1" class="table">
  		<tr>
  			<th>ID</th>
  			<th>Nama</th>
  			<th>Email</th>
  			<th>Alamat</th>
  			<th>Aksi</th>
  		</tr>
  		<?php
  		include "koneksi.php";
  		$query_mysql = mysql_query("SELECT * FROM customer")or die(mysql_error());
  		$nomor = 1;
  		while($data = mysql_fetch_array($query_mysql)){
  		?>
  		<tr>
        <td><?php echo $data['id']; ?></td>
  			<td><?php echo $data['name']; ?></td>
  			<td><?php echo $data['email']; ?></td>
  			<td><?php echo $data['addresss']; ?></td>
  			<td>
  				<a class="edit" href="6-customer-ubah.php?id=<?php echo $data['id']; ?>">Edit</a> |
  				<a class="hapus" href="6-customer-hapus.php?id=<?php echo $data['id']; ?>">Hapus</a>
  			</td>
  		</tr>
  		<?php } ?>
  	</table>

  </body>
</html>
